/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package net.customware.confluence.plugin.google.calendar.legacy;

import org.randombits.support.confluence.legacy.LegacyConfluenceMacro;
import org.randombits.support.core.env.EnvironmentAssistant;

import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.RenderMode;

/**
 * TODO: Document this class.
 * 
 * @author David Peterson
 */

/*
 * <iframesrc=
 * "http://www.google.com/calendar/hosted/randombits.org/embed?title=MyTitle&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=david%40randombits.org&amp;color=%23BE6D00&amp;ctz=Australia%2FBrisbane"
 * style=" border-width:0 " width="800" height="600" frameborder="0"
 * scrolling="no"></iframe>
 */
public class GoogleCalendarMacro extends LegacyConfluenceMacro {
    private static final RenderMode RENDER_MODE = RenderMode.suppress( RenderMode.F_FIRST_PARA );

    private Macro macro;

    public GoogleCalendarMacro(EnvironmentAssistant environmentAssistant, XhtmlContent xhtmlContent) {
    	macro = new net.customware.confluence.plugin.google.calendar.GoogleCalendarMacro(environmentAssistant, xhtmlContent);
    }

    @Override
    protected Macro getXHtmlMacro() {
            return macro;
    }

    public RenderMode getBodyRenderMode() {
            return RENDER_MODE;
    }

}
