package net.customware.confluence.plugin.google.calendar;

import com.atlassian.confluence.macro.xhtml.RichTextMacroMigration;

public class GoogleCalendarMacroMigrator extends RichTextMacroMigration{
	public GoogleCalendarMacroMigrator (com.atlassian.confluence.macro.xhtml.MacroManager xhtmlMacroManager) {
	 	super(xhtmlMacroManager);
	}
}
